libflickr-api-perl (1.29-1) unstable; urgency=medium

  * Import upstream version 1.29.
  * Drop method-not-found.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Mar 2024 01:37:33 +0100

libflickr-api-perl (1.28-4) unstable; urgency=medium

  * Add patch for changed Flickr error code. (Closes: #1064755)
  * Disable DNS queries during tests as network access is forbidden by
    Debian policy.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Drop unneeded alternative dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Wrap long lines in changelog entries: 1.28-3.

 -- gregor herrmann <gregoa@debian.org>  Mon, 26 Feb 2024 00:02:39 +0100

libflickr-api-perl (1.28-3) unstable; urgency=medium

  [ Xavier Guimard ]
  * Email change: Xavier Guimard -> yadd@debian.org

  [ gregor herrmann ]
  * debian/*: replace ADTTMP with AUTOPKGTEST_TMP.
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.2.1, no changes needed.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 10 Dec 2022 22:38:13 +0000

libflickr-api-perl (1.28-2) unstable; urgency=medium

  * Team upload

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Alex Muntada ]
  * Fix POD issues; remove .pl from scripts POD.
    Closes: #875835

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Damyan Ivanov ]
  * describe remove-dot-pl-from-scripts-pod.patch
  * bump debhelper compatibility level to 11
  * patch a couple of POD spelling mistakes
  * Declare conformance with Policy 4.2.0 (no changes needed)

 -- Damyan Ivanov <dmn@debian.org>  Tue, 14 Aug 2018 16:56:29 +0000

libflickr-api-perl (1.28-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Martín Ferrari ]
  * Remove myself from Uploaders.

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jose Luis Rivas from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Import upstream version 1.28
  * Update years of upstream copyright
  * Refresh patch (line offset)
  * Ship provided examples and docs
  * Declare compliance with Debian Policy 3.9.8

 -- Florian Schlichting <fsfs@debian.org>  Wed, 19 Oct 2016 17:51:06 +0200

libflickr-api-perl (1.27-1) unstable; urgency=medium

  * Import upstream version 1.27.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Dec 2015 21:45:30 +0100

libflickr-api-perl (1.26-1) unstable; urgency=medium

  * Import upstream version 1.26.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Nov 2015 17:07:55 +0100

libflickr-api-perl (1.25-1) unstable; urgency=medium

  * Import upstream version 1.25.
  * Update fix_script_path_autopkgtest.patch.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Oct 2015 16:04:28 +0200

libflickr-api-perl (1.19-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.19
  * Remove .pl extension from flickr_make_test_values.pl during installation

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 21 Sep 2015 11:21:34 -0300

libflickr-api-perl (1.18-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.18
  * Remove spelling errors patch. It is already applied in upstream.

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Mon, 17 Aug 2015 12:50:07 -0300

libflickr-api-perl (1.16-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 1.16
  * Update dependencies
  * Declare compliance with Debian Policy 3.9.6
  * Add autopkgtest
  * Remove .pl from executables and manpages when install them
  * Add patch to fix spelling error in manpage (silence lintian)
  * Set debhelper >= 9
  * Update upstream copyright
  * Patch tests to run them in installed version (autopkgtest)

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 25 Jul 2015 15:38:13 -0300

libflickr-api-perl (1.10-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Jul 2014 20:39:10 +0200

libflickr-api-perl (1.09-1) unstable; urgency=medium

  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Jun 2014 17:10:37 +0200

libflickr-api-perl (1.08-1) unstable; urgency=low

  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Oct 2013 23:13:54 +0200

libflickr-api-perl (1.07-1) unstable; urgency=low

  * New upstream release.
    Fixes "Encoding bug within libflickr-api-perl (utf8)"
    (Closes: #709476)

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 May 2013 11:31:42 +0200

libflickr-api-perl (1.06-1) unstable; urgency=low

  * Imported Upstream version 1.06
  * Update debian/copyright years
  * Add libhttp-message-perl dependency

 -- Xavier Guimard <x.guimard@free.fr>  Thu, 16 May 2013 05:38:50 +0200

libflickr-api-perl (1.05-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Relicensed under Perl Artistic v 2.0
  * Added myself to Uploaders, updated Copyright
  * Added full text of Artistic2 to copyright; not in common-licenses

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Email change: Jonathan Yu -> jawnsy@cpan.org

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Xavier Guimard ]
  * Imported Upstream version 1.05
  * Update source format to 3.0 (quilt)
  * Bump Standards-Version to 3.9.4
  * Use debhelper 8
  * Remove libcompress-zlib-perl from dependencies (now included in Perl)
  * Update debian/copyright (years and format)
  * Remove "--with-quilt" old arg from debian/rules
  * Remove patch unneeded now

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

 -- Xavier Guimard <x.guimard@free.fr>  Mon, 01 Apr 2013 07:46:12 +0200

libflickr-api-perl (1.01-3) unstable; urgency=low

  * Drop patch should_query_for_tags_not_elements, breaks with newer
    libxml-parser-lite-tree-perl. Bump versioned dependency on
    libxml-parser-lite-tree-perl to >= 0.08. Thanks to Mark Broadbent for the
    bug report. Closes: #504481
  * debian/copyright: refresh formatting, update years of upstream copyright.
  * Set debhelper compatibility level to 7; adapt
    debian/{control,compat,rules}. Drop cdbs from build dependencies.
  * Add /me to Uploaders.
  * Don't install almost empty README any more.
  * debian/watch: extended regexp for matching upstream releases.

 -- gregor herrmann <gregoa@debian.org>  Tue, 04 Nov 2008 17:56:51 +0100

libflickr-api-perl (1.01-2) unstable; urgency=low

  * Switched over the patch system to quilt
  * Fixed bug that called the wrong method on the API, resulting in many
    operations being unperformable (Closes: #5023312)

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 17 Oct 2008 18:52:36 -0500

libflickr-api-perl (1.01-1) unstable; urgency=low

  [ Gunnar Wolf ]
  * New upstream release
  * Standards-version -> 3.8.0 (no changes needed)

  [ Martín Ferrari ]
  * Updating my email address

 -- Gunnar Wolf <gwolf@debian.org>  Sun, 12 Oct 2008 12:21:11 -0500

libflickr-api-perl (0.09-1) unstable; urgency=low

  [ Damyan Ivanov ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza).

  [ gregor herrmann ]
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Martín Ferrari ]
  * New upstream release.
  * debian/watch: use dist/ URL.
  * debian/control:
  - remove unneeded reference to perldoc in the web.
  - add myself to Uploaders and set Maintainer to DPG.
  - bump Standards-Version (no changes).
  - remove spurious dependency on libsoap-lite-perl.
  - added version to libxml-parser-lite-tree-perl dependency.
  * debian/copyright: new format, and real CP info (it is Artistic and not
    GPL!), changed packaging license (José asked me to do it for him).

  [ Niko Tyni ]
  * Depend on libwww-perl and liburi-perl, recommend libcompress-zlib-perl
  * debian/patches/test-no-internet: really disable all internet connections
                                     in the test suite. (Closes: #465431)
  * Remove unnecessary debian/dirs.

  [ Gunnar Wolf ]
  * Bumped up debhelper compat level to 6
  * Added myself as an uploader

 -- Gunnar Wolf <gwolf@debian.org>  Fri, 29 Feb 2008 17:04:57 -0600

libflickr-api-perl (0.08-2) unstable; urgency=low

  * debian/patches/00-fix427318-jun3-07.test.pl.patch:
   + Created, now don't use internet connection. (Closes: #427318)
  * debian/rules:
   + Added line for using `simple-patchsys.mk' CDBS rule.

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Sun, 03 Jun 2007 16:37:29 -0400

libflickr-api-perl (0.08-1) unstable; urgency=low

  * Initial release (Closes: #420097)

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Thu, 19 Apr 2007 17:44:20 -0400
